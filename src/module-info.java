/**
 * <p>Este modulo contiene un programa funcional que trabaja con una lista de la compra,
 * el cual será capaz de guardar los nombres de los productos así como sus cantidades, realizando:</p>
 * <ul>
 * 		<li>Añadir productos a la cesta</li>
 * 		<li>Modificar productos de la cesta</li>
 * 		<li>Eliminar productos de la cesta</li>
 * 		<li>Ordenar y mostrar alfabéticamente los productos de la cesta</li>
 * </ul>
 * @author Christian
 * @version 1.0
 * @since 1.0
 *
 */

module org.villablanca {
	exports org.villablanca;
}