package org.villablanca;

/**
 * Clase con funciones que sirven para crear una <strong>cesta de la compra</strong>
 * la cual va ha tener 2 arrays, uno para los nombres de los productos (String) y otro para las cantidades de los mismos (int).
 * En la misma se van a poder aÃ±adir, modificar, eliminar y mostrar los productos, asi como la lista completa de ellos.
 * @author Diego
 */
public class Main {
	
	/**
	 * Creo la función MostrarMenu()
	 * La función no devuelve ningún valor. 
	 * Solo imprime por pantalla las opciones del menú.
	 */
	public static void mostrarMenu() {
			System.out.println("Elige una opción:\n"
					+ "1.- Añadir producto"
					+ "2.- Eliminar producto"
					+ "3.- Calcular media productos"
					+ "4.- Salir");
		}

	/***
	 * Atributos globales que representan una lista de la compra con la cantidad de productos (array int) y
	 * nombre de productos (array String).
	 * Entero que contabiliza el numero de productos en la cesta.
	 */
	private static int cantidadProductos[];
	private static String productos[];
	private static int contadorProductos;
	
	/**
	 * Funcion que agrega un producto a la cesta (tanto el nombre como la cantidad)
	 * siempre y cuando el producto no exista en la cesta, si existe se llama a modificarProducto()
	 * @param productos Array de String de los nombres de los productos
	 * @param cantidadProductos Array de int de las cantidades de cada producto
	 * @param numeroProductos numero de productos totales en la cesta
	 * @return Cantidad de productos totales en la cesta actualizado
	 */
	public static int addProducto(String[] productos, int[] cantidadProductos, int numeroProductos) {
		int cantidad=0;
		String producto = "";
		System.out.print("Â¿Cuantos productos vas a aÃ±adir? ");
		int add = Teclado.leerInt();
		for(int i = numeroProductos; i <= numeroProductos+add; i++) {
			System.out.print("Nombre del producto: ");
			producto = Teclado.leerString();
			int posicionProducto = buscarProducto(productos, producto, numeroProductos);
			if (posicionProducto != -1) {
				do {
					System.out.print("Cantidad de " + producto + " a aÃ±adir: ");
					cantidad = Teclado.leerInt();
				} while (cantidad <= 0);
				productos[i] = producto;
				cantidadProductos[i] = cantidad;
			} else {
				modificarProducto(cantidadProductos, posicionProducto);
			}
			
		}
		return numeroProductos + add;
	}
	
	/**
	 * Funcion que intercambia los valores de ambos arrays
	 * @param productos Array de String a modificar
	 * @param cantidadProductos Array de int a modificar
	 * @param este indice/Posicion de ambos array a intercambiar por "int porEste"
	 * @param porEste indice/Posicion de ambos array que sera intercambiado por "int este"
	 */
	public static void intercambiarValores(String[] productos, int[] cantidadProductos, int este, int porEste) {
		String aux1 = productos[este];
		int aux2 = cantidadProductos[este];
		
		productos[este] = productos[porEste];
		cantidadProductos[este] = cantidadProductos[porEste];
		
		productos[porEste] = aux1;
		cantidadProductos[porEste] = aux2;
	}
	
	/**
	 * Funcion que ordena alfabeticamente los arrays de la cesta de la compra
	 * @param productos Array de String de los nombres de los productos
	 * @param cantidadProductos Array de int de las cantidades que hay en la cesta de cada producto
	 */
	public static void ordenarAlfabeticamente(String[] productos, int[] cantidadProductos) {
		boolean hayDesorden = true;
		for(int i = 0; i < productos.length && i < cantidadProductos.length && hayDesorden; i++) {
			hayDesorden = false;
			for (int j = 1; j < productos.length; j++) {
				if (productos[j-1].compareToIgnoreCase(productos[j]) < 0) {
					intercambiarValores(productos, cantidadProductos, j-1, j);
				}
			}
		}
	}
	
	/**
	 * Función que muestra por pantalla la lista de la compra (previamente ordenada alfabeticamente)
	 * @param productos Array (String) de los nombres de los productos
	 * @param cantidadProductos Array (int) de las cantidades de cada producto
	 * @param numeroProductos Numero total de productos en la cesta
	 */
	public static void mostrarCesta(String[] productos, int[] cantidadProductos, int numeroProductos) {
		ordenarAlfabeticamente(productos, cantidadProductos);
		System.out.println("Lista total de la cesta:");
		for(int i = 0; i < productos.length && i < cantidadProductos.length; i++) {
			System.out.println(productos[i] + " -> Cantidad: " + cantidadProductos[i] + "uds.");
		}
	}

	/**
	 * Crear función buscar producto.
	 * @param nombres recibe un array de tipo string.
	 * @param buscar recibe el String para buscar en el array.
	 * @param contadorProductos recibe el numero de productos.
	 * @return devuelve la poscion actual del producto buscado, si no hay coincidencias devuelve -1.
	 */
	public static int buscarProducto(String[] nombres, String buscar, int contadorProductos) {
		int posicion = -1;
		for(int i = 0; i < contadorProductos && i < nombres.length; i++) {
			if(nombres[i].equalsIgnoreCase(buscar)) {
				posicion = i;
			}
		}
		return posicion;
	}
	
	/***
	 * Funcion que modifica la cantidad de productos.
	 * @param cantidadProductos array cantidad de productos.
	 * @param posicionProducto posicion del producto que modifica. 
	 */
	public static void modificarProducto(int[] cantidadProductos, int posicionProducto) {
		int cantidad;
		boolean error = false;
		System.out.println("Este producto ya existe en la cesta, �que deseas realizar?");
		int opcion = Teclado.leerInt("1) A�adir cantidad\n2)Eliminar cantidad");
		
		switch (opcion) {
		case 1:
			cantidad = Teclado.leerInt("Introduce cantidad");
			cantidadProductos[posicionProducto] += cantidad;
			break;
		case 2:
			do {
				error = false;
				cantidad = Teclado.leerInt("Introduce cantidad");
				
				if (cantidadProductos[posicionProducto] - cantidad < 0) {
				System.out.println("No se puede realizar la operacion");
				error = true;
				}
				cantidadProductos[posicionProducto] -= cantidad;
				
			} while(error);
			break;
			
		default:
			System.out.println("Opcion incorrecta");
			break;
		}
		
	}
	
	/**
	 * Funcion que mueve un producto en la lista despues de eliminarlo para evitar huecos vacios
	 * @param productos Array de nombres de productos
	 * @param cantidadProductos Array de cantidades de productos
	 * @param anterior posicion anterior en el array
	 * @param siguiente posicion siguiente en el array
	 */
	public static void moverProducto(String[] productos, int[]cantidadProductos, int anterior, int siguiente) {
		String auxNombre = productos[anterior];
		productos[anterior] = productos[siguiente];
		productos[siguiente] = auxNombre;
		int auxCantidad = cantidadProductos[anterior];
		cantidadProductos[anterior] = cantidadProductos[siguiente];
		cantidadProductos[siguiente] = auxCantidad;
	}
	
	/**
	 * Funcion que elimina un producto y sus cantidades de la cesta
	 * @param productos Array de nombres de productos
	 * @param cantidadProductos Array de cantidades de productos
	 * @param cantidadTotalProductos Cantidad total de productos en la cesta
	 * @return cantidad total de productos actualizada
	 */
	public static int deleteProducto(String[] productos, int[] cantidadProductos, int cantidadTotalProductos) {
		String nombre = "";
		int posProducto = 0;
		System.out.print("Introduzca el nombre del producto a eliminar de la cesta: ");
		nombre = Teclado.leerString();
		posProducto = buscarProducto(productos, nombre, cantidadTotalProductos);
		if (posProducto != -1) {
			productos[posProducto] = "";
			cantidadProductos[posProducto] = 0;
			moverProducto(productos, cantidadProductos, posProducto, cantidadTotalProductos-1);
			System.out.println("Has eliminado " + nombre + " de tu cesta.");
		} else {
			System.out.println("Error, el producto indicado no está en tu cesta.");
		}
		
		return cantidadTotalProductos - 1;
	}
	
	/**
	 * Función que calcula la media de cantidad de productos
	 * @param productos Array de nombres de productos
	 * @param cantidadProductos Array de cantidades de productos
	 * @param contadorProductos Número total de productos
	 */
	public static void calcularMedia(String[] productos, int[] cantidadProductos, int contadorProductos) {
		double media = 0.0;
		for(int i = 0;i<cantidadProductos.length;i++) {
			media += (double)cantidadProductos[i];
		}
		media = media / contadorProductos;
		System.out.println("La media de cantidad de productos totales es: " + media);
	}
	
	/***
	 * Funcion que ejecuta las opciones del menu principal.
	 * @param opcion Opción seleccionada en el menu principal.
	 * @param productos array nombres de productos.
	 * @param cantidadProductos array cantidad de productos. 
	 * @param contadorProductos cantidad de productos totales. 
	 */
	public static void ejecutarOpciones(int opcion, String[] productos, int[] cantidadProductos, int contadorProductos) {
		switch(opcion) {
		case 1:
			addProducto(productos, cantidadProductos, contadorProductos);
			break;
		case 2:
			deleteProducto(productos, cantidadProductos, contadorProductos);
			break;
		case 3:
			calcularMedia(productos, cantidadProductos, contadorProductos);
			break;
		case 4:
			System.out.println("Programa finalizado");
			break;
		default:
			System.out.println("Opcion incorrecta");
			break;
		}
	}
	
	/**
	 * Método principal que ejecuta el programa
	 * @param args Argumentos de la terminal
	 */
	public static void main(String[] args) {
		int opcion = 0;
		do {
			mostrarMenu();
			System.out.println("Introduce opcion: ");
			opcion = Teclado.leerInt();
			ejecutarOpciones(opcion, productos, cantidadProductos, contadorProductos);
		} while(opcion != 4);
	}

}
