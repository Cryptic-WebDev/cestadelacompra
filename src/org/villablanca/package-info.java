/**
 * <p>El paquete org.villablanca pertenece al alumnado del Ciclo Formativo de Grado Superior Desarrollo de Aplicaciones Web</p>
 * <p>El equipo que ha realizado este proyecto son:</p>
 * <ul>
 * 		<li>Christian (Scrum Master)</li>
 * 		<li>Laura (Developer)</li>
 * 		<li>Diego (Developer)</li>
 * 		<li>Blas (Developer)</li>
 * 		<li>Carlos (Developer)</li>
 * 		<li>Sergio (Developer)</li>
 * 		<li>Ivan (Developer)</li>
 * </ul>
 */

package org.villablanca;