package org.villablanca;

import java.util.Scanner;

/**
 * Clase que sirve para leer datos por pantalla/terminal
 * @author Christian
 *
 */
public class Teclado {
	
	/**
	 * Lee por teclado una cadena de caracteres
	 * @return Una cadena de caracteres
	 */
	public static String leerString () {
		Scanner teclado = new Scanner(System.in);
		return teclado.nextLine();
	}
	
	/**
	 * Lee por teclado una cadena de caracteres y muestra un mensaje al usuario
	 * 	@param frase Mensaje para pedir el dato al usuario 
	 * @return Una cadena de caracteres
	 */
	public static String leerString (String frase) {
		Scanner teclado = new Scanner(System.in);
		System.out.print(frase + " ");
		return teclado.nextLine();
	}
	
	/**
	 * Lee por teclado un entero
	 * @return Un número entero
	 */
	public static int leerInt () {
		Scanner teclado = new Scanner(System.in);
		return teclado.nextInt();
	}
	
	/**
	 * Lee por teclado un entero y muestra un mensaje al usuario
	 * 	@param frase Mensaje para pedir el dato al usuario 
	 * @return Un número entero
	 */
	public static int leerInt (String frase) {
		Scanner teclado = new Scanner(System.in);
		System.out.print(frase + " ");
		return teclado.nextInt();
	}
	
	/**
	 * Lee por teclado un real
	 * @return Un número real
	 */
	public static double leerDoble() {
		Scanner teclado = new Scanner(System.in);
		return teclado.nextDouble();
	}
	
	/**
	 * Lee por teclado un real y muestra un mensaje al usuario
	 * 	@param frase Mensaje para pedir el dato al usuario 
	 * @return Un número real
	 */
	public static double leerDoble (String frase) {
		Scanner teclado = new Scanner(System.in);
		System.out.print(frase + " ");
		return teclado.nextDouble();
	}
	
	/**
	 * Lee por teclado un caracter
	 * @return Un caracter
	 */
	public static char leerChar() {
		Scanner teclado = new Scanner(System.in);
		return teclado.nextLine().charAt(0);
	}
	
	/**
	 * Lee por teclado un caracter y muestra un mensaje al usuario
	 * @param frase Mensaje para pedir el dato al usuario 
	 * @return Un caracter
	 */
	public static char leerChar (String frase) {
		Scanner teclado = new Scanner(System.in);
		System.out.print(frase + " ");
		return teclado.nextLine().charAt(0);
	}
	
}
